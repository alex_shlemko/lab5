#pragma once
#include"List.h"
#include <string>
using namespace std;
class TriangleList: public List
{
public:
	TriangleList();
	TriangleList(double thicknessC, double densityC, double side1C, double side2C);
	~TriangleList();

	virtual double getArea()const;
	virtual void Info();
private:

	string name;
};


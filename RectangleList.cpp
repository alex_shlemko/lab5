#include "RectangleList.h"



RectangleList::RectangleList()
{
}

RectangleList::RectangleList(double thicknessC, double densityC, double side1C, double side2C):List(thicknessC, densityC,side1C, side2C) {
	
	name = "Rectangle List";
}

RectangleList::~RectangleList()
{

}

double RectangleList::getArea()const {
	return width*length;
}

void RectangleList::Info() {
	cout << "Name: " << name<<endl;
	List::Info();
}

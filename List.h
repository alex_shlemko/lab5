#pragma once
#include<iostream>
using namespace std;
class List
{

public:
	List();
	List(double thicknessC, double densityC, double side1C, double side2C);
virtual	~List();
	 virtual double getArea()const = 0;
	 double getWeight()const;
	 

	 friend ostream &operator<<(ostream&os, List& li);

	 virtual void Info();
protected:
	double thickness;
	double density;
	double width;
	double length;
};


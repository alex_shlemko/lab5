#pragma once
#include "List.h"
#include<string>
using namespace std;

class SquareList : public List
{
public:
	
	SquareList();
	SquareList(double thicknessC, double densityC, double sideC );
	~SquareList();

	virtual double getArea()const;

	virtual void Info();
	
private:
	string name;
};


#include "List.h"
#include<iostream>


List::List()
{
	thickness = 0;
	density = 0;
	width = 0;
	length = 0;
}

List::List(double thicknessC, double densityC, double side1C, double side2C) {
	thickness = thicknessC/1000;
	density = densityC;
	width = side1C/1000;
	length = side2C/1000;

}

List::~List()
{

}



double List::getWeight()const {
	 float res= getArea()*density*thickness;
	return res;
}


ostream& operator<<(ostream& os, List& li) {
	os << "Size: " << li.width << "*" << li.length << endl
		<< "Area:" << li.getArea() << endl
		<< "Weight: " << li.getWeight() << endl
		<< "Thickness: " << li.thickness << endl
		<< "Density: " << li.density << endl;
	return os;
}


void List::Info() {
	cout << *this;
}

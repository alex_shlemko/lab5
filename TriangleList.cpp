#include "TriangleList.h"

using namespace std;

TriangleList::TriangleList():List()
{
}

TriangleList::TriangleList(double thicknessC, double densityC, double side1C, double side2C): List(thicknessC, densityC, side1C, side2C)
{
	name = "Triangle list";
	
}

TriangleList::~TriangleList()
{
}


double TriangleList::getArea()const {
	return 0.5*width*length;
}

void TriangleList::Info() {
	cout <<"Name: "<<name<<endl;
	List::Info();
}
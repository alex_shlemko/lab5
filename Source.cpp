#include <iostream>
#include<string>
#include<vector>
#include"List.h"
#include"SquareList.h"
#include"TriangleList.h"
#include "RectangleList.h"
#include<time.h>
using namespace std;



int main() {
	srand(time(NULL));
	vector<List*> lists;

	for (size_t i = 0; i < 15; i++)
	{
		if (i < 5)lists.push_back(new SquareList(rand(), 4768, rand()));
		if (i >= 5 && i < 12)lists.push_back(new RectangleList(rand(), 4768, rand(), rand()));
		if(i>=12)lists.push_back(new TriangleList(rand(), 4768, rand(), rand()));
		
	}

	double totalArea = 0;
	double totalWeight = 0;

	for (size_t i = 0; i < lists.size();i++)
	{	
		totalArea += lists[i]->getArea();
		totalWeight += lists[i]->getWeight();
		lists[i]->Info();
		cout << endl;
		cout << endl;
	}

	cout << "Total area: " << totalArea << endl;
	cout << "Total weight: " << totalWeight << endl;

	system("pause");
	return 0;
}
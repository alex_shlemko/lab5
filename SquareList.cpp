#include "SquareList.h"



SquareList::SquareList()
{
	
	name = "";
	width =0;
	length = 0;
}


SquareList::SquareList(double thicknessC, double densityC, double sideC): List(thicknessC, densityC, sideC, sideC)
{	

	name = "Square List";
	
}

SquareList::~SquareList() {

}

 double  SquareList::getArea()const {
	return width*length;
}  

void SquareList::Info() {
	cout << "Name: " << name << endl;
	List::Info();
}
#pragma once
#include "List.h"
#include<string>
#include<iostream>
class RectangleList : public List
{
public:
	RectangleList();
	RectangleList(double thicknessC, double densityC, double side1C, double side2C);
	~RectangleList();

	virtual double getArea()const;

	virtual void Info();

	

private:


	string name;
	
};

